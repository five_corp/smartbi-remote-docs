Datos de Configuración
======================

La configuración de todos los componentes del servicio reside en ficheros de texto en el directorio *C:\\smartBI\\config**.

Los ficheros de configuración se crean automáticamente al ejecutar el instalador, a partir de plantillas. El instalador aplica las plantillas, reemplazando las variables por los parámetros detectados en tiempo de instalación.

Tras la instalación, generalmente no es necesario modificar los ficheros de configuración durante el tiempo de vida de la aplicación. Las plantillas de cada fichero de configuración son:

.. _callhome_yaml:

configs/callhome.yaml
---------------------

.. literalinclude:: configs/callhome.tmpl
   :language: yaml
   :linenos:

.. _cert_yaml:

configs/cert.yaml
-----------------

.. literalinclude:: configs/cert.tmpl
   :language: yaml
   :linenos:

.. _command_yaml:

configs/command.yaml
--------------------

.. literalinclude:: configs/command.tmpl
   :language: yaml
   :linenos:

.. _database_xml:

configs/database.xml
--------------------

.. literalinclude:: configs/database.tmpl
   :language: xml
   :linenos:

.. paths_xml:

configs/paths.xml
-----------------

.. literalinclude:: configs/paths.tmpl
   :language: xml
   :linenos:

.. _queries_yaml:

configs/queries.yaml
--------------------

.. literalinclude:: configs/queries.tmpl
   :language: yaml
   :linenos:

.. _schedules_yaml:

configs/schedules.yaml
----------------------

.. literalinclude:: configs/schedules.tmpl
   :language: yaml
   :linenos:

.. _server_yaml:

configs/server.yaml
-------------------

.. literalinclude:: configs/server.tmpl
   :language: yaml
   :linenos:

.. _token_yaml:

configs/token.yaml
------------------

.. literalinclude:: configs/token.tmpl
   :language: yaml
   :linenos:

.. _update_yaml:

configs/update.yaml
-------------------

.. literalinclude:: configs/update.tmpl
   :language: yaml
   :linenos:

.. _uploads_yaml:

configs/uploads.yaml
--------------------

.. literalinclude:: configs/uploads.tmpl
   :language: yaml
   :linenos:

.. _serman_yaml:

service/svc/serman.yaml
-----------------------

.. literalinclude:: configs/serman.tmpl
   :language: yaml
   :linenos:

.. _service_yaml:

service/current/service.yaml
----------------------------

.. literalinclude:: configs/service.tmpl
   :language: yaml
   :linenos:

