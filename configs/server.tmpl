---
# Server.yaml: Parámetros del servidor HTTPS REST

version:       {{ .Version }}

# Activar HTTPS
https:         true

# Puerto de escucha
port:          {{ .Config.Port }}

# Timeouts de lectura y escritura de paquetes en la red.
# Ampliados a un tiempo lo bastante largo para poder subir
# excels grandes...
readTimeout:   {{ or .Config.ReadTimeout 300 }}
writeTimeout:  {{ or .Config.WriteTimeout 300 }}

# Máximo tamaño de cabecera HTTP(S) (kbytes)
maxHeaderSize: {{ or .Config.MaxHeaderSize 4 }}

# Máximo tamaño de cuerpo HTTPS (megabytes)
maxBodySize:   {{ or .Config.MaxBodySize 32 }}

# Máxima cantidad de memoria a reservar por petición (megabytes)
maxFileMemory: {{ or .Config.MaxFileMemory 32 }}

# Tiempo Maximo a mantener respuestas en cache (segundos)
maxAge:        {{ or .Config.MaxAge 30 }}

# Numero de Etags a guardar en cache
etagCache:     {{ or .Config.EtagCache 8192 }}

# Prefijo de todas las rutas de la API
prefix:        "/remote/api"

# Ruta por defecto, si no se especifica ninguna
redirect:      "/status"
