.. smartBI-remote documentation master file, created by
   sphinx-quickstart on Tue Aug 02 08:46:07 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Aplicación smartBI
==================

Este documento contiene la descripción de los componentes de la aplicación smartBI que se instalan en el servidor del cliente, así como los procedimientos para su instalación, configuración, actualización y desinstalación.

Contenidos:

.. toctree::
   :maxdepth: 2
   :numbered:
   
   intro
   install
   config
   logs
   update
   uninstall