Instalación
===========

Prerequisito MySQL
------------------

La aplicación requiere una base de datos MySQL versión **5.7.7 o superior**. La base de datos debe estar preinstalada en el servidor del usuario, y la contraseña del usuario root debe ser conocida.

No es necesario que la contraseña de root sea la misma para todos los clientes; es suficiente con que se proporcione la contraseña correcta durante el proceso de configuración de la aplicación smartBI en el servidor de cada cliente.

MySQL puede ser instalado de forma desatendida, mediante un paquete .zip [#noinstall]_ que se despliega y configura sin necesidad de instalador gráfico ni interacción del usuario. Este proceso utiliza un fichero de opciones [#options]_ que especifica tanto las rutas de instalación y almacenamiento de datos, como las opciones de funcionamiento. `Esta URL <_static/my.ini>`_ permite descargar un fichero de opciones tipo para MySQL 5.7.

.. [#noinstall] http://dev.mysql.com/doc/refman/5.7/en/windows-install-archive.html
.. [#options] http://dev.mysql.com/doc/refman/5.7/en/windows-create-option-file.html

Zonas horarias
--------------

Como parte del despliegue de MySQL, en entornos Windows es necesario cargar una tabla de zonas horarias para que las operaciones relacionadas con campos tipo fecha soporten la hora local [#timezone]_ . La carga de franjas horarias requiere:

- Descargar en el servidor del usuario el fichero de `zonas horarias <http://downloads.mysql.com/general/timezone_2016f_posix_sql.zip>`_.
- Descomprimir el archivo descargado extrayendo el fichero **timezone_posix.sql**.
- Importar el fichero en mysql con la orden:

  ``mysql -u root mysql < timezone_posix.sql``

  Se recomienda que la carga del fichero de franjas horarias se realice **antes** de establecer el password de root de MySQL. En caso contrario, será necesario proporcionar el password del usuario en la línea de comandos:

  ``mysql -u root -p mysql < timezone_posix.sql``

.. [#timezone] https://dev.mysql.com/doc/refman/5.7/en/time-zone-support.html

Despliegue desatendido
----------------------

El resto de módulos de la aplicación se despliega de forma desatendida. Una vez satisfechos los prerrequisitos, el procedimiento de despliegue y configuración inicial de la aplicación es el siguiente:

1. Descompresión de los paquetes de distribución de cada componente, proporcionados por Five, en la ruta correspondiente:

  ================ =========================== ====================
  Componente       Fichero de distribucion     Ruta
  ================ =========================== ====================
  Java JRE 8       java8.zip [#java8.zip]_     C:\\smartBI\\java8
  Pentaho          pentaho.zip [#pentaho.zip]_ C:\\smartBI\\pentaho
  Jobs             jobs.zip [#jobs.zip]_       C:\\smartBI\\jobs
  Serman, Uploader service.zip [#service.zip]_ C:\\smartBI\\service
  ================ =========================== ====================

.. [#java8.zip] https:/smartbi.acens.net/public/java8.zip
.. [#pentaho.zip] https:/smartbi.acens.net/public/pentaho.zip
.. [#jobs.zip] https:/smartbi.acens.net/public/jobs.zip
.. [#service.zip] https:/smartbi.acens.net/public/service.zip

2. Configuración inicial de la aplicación, mediante la ejecución del asistente **bootstrap.exe** (localizado en C:\\smartBI\\service\\current), con los siguientes parámetros.

  - **-host**: Dirección IP del servidor MySQL. Por defecto, “localhost”.
  - **-port**: Puerto TCP del servicio MySQL. Recomendado 13306.
  - **-restport**: Puerto TCP para el servicio Uploader. Recomendado 3000.
  - **-password**: Contraseña de usuario root del servicio MSQL.

  La aplicación bootstrap.exe debe ejecutarse una vez que la base de datos MySQL está arrancada, ya que se conectará a ella para crear el esquema correspondiente, o para actualizarlo si ya existe.

  El asistente devuelve un valor de salida **0** si la configuración se completa con éxito, o distinto de cero en otro caso.

3. Instalación del servicio mediante la orden ``serman.exe -service install`` (el ejecutable **serman.exe** está localizado en c:\\smartBI\\service\\svc). Esta orden crea un nuevo servicio de Windows, con el nombre **smartBI**, y configurado para arrancar automáticamente con el inicio del sistema operativo.

4. Inicio del servicio smartBI. Se puede utilizar cualquier herramienta habitual para arrancar el servicio por primera vez, como el comando ``net start smartBI``, o se puede utilizar de nuevo la aplicación *serman* con la orden ``serman.exe -service start``.

Apertura de puertos
-------------------

Para su correcto funcionamiento, la aplicación necesita que los servicios de **Uploader** y **MySQL** sean accesibles desde el servidor o servidores de backend donde se ejecutan los procesos de gestión de usuarios, licencias, etc. Por lo tanto, los puertos de ambos servicios deben estar abiertos en el Firewall de Windows.

Para evitar conflictos con otras aplicaciones que el cliente pueda instalar en el servidor, y para que esos puertos no sean susceptibles de escaneo de números de puerto conocidos por parte de atacantes externos, se recomienda:

- Cambiar los números de puerto estándar:

  - Usar el puerto **13306** para MySQL, en lugar de 3306.
  - Usar el puerto **3000** para Uploader, en lugar de 443.

- Filtrar el acceso a esos puertos mediante el Firewall de Windows, permitiendo el acceso sólo desde el servidor o servidores de backend.

Cabe destacar que el acceso al servicio es cifrado mediante TLS y autenticado con claves secretas y exclusivas para cada suscriptor de smartBI.

