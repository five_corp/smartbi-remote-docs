Introducción
============

Componentes
-----------

La aplicación smartBI hace uso de múltiples tecnologías: herramienta de ETL basadas en Java, bases de datos multiplataforma como MySQL [#mysql]_, y lenguajes que facilitan la ejecución de tareas concurrentes como node [#node]_ o go [#go]_. Los componentes de la aplicación se enumeran a continuación.

- **Uploader**: Es el servidor HTTPS que implementa la API REST mediante la cual se gestionan el resto de componentes.
- **Serman**: Es el gestor de servicios que permite controlar el servidor REST como un servicio de Windows.
- **Pentaho**: Es el motor de transformación ETL que se ejecuta bajo demanda cuando los clientes cargan nuevos datos para procesar.
- **Jobs**: Es la lista de trabajos ETL que se ejeuctan en el motor de transformación (Pentaho) para extraer, transformar y cargar los datos proporcionados por el cliente.
- **MySQL**: Es el motor de base de datos elegido para albergar los datos procesados por el motor de transformación.

.. topic:: Uploader y Serman

  Estos dos programas son desarrollos propios de Five, distribuidos como ficheros ejecutables compilados estáticamente sin dependencias externas (no requieren .Net, VC, DLLs…). Ambos deben estar en ejecución permanentemente para el correcto funcionamiento de la aplicación.

  El impacto de estos servicios sobre la máquina del cliente es mínimo tanto en términos de CPU como de memoria, consumiendo típicamente del orden de 20 MBytes de RAM y menos del 1% de CPU.

.. topic:: Pentaho

  Esta aplicación está basada en un producto opensource (Pentaho Data Integration [#pentaho]_), modificado por Five, y requiere de un entorno Java JRE versión 8 para su ejecución.

  El motor de transformación se lanza bajo demanda, únicamente cuando el usuario introduce datos nuevos, lo que típicamente puede ocurrir con frecuencia semanal. Solo en ese momento consume recursos del servidor.

.. topic:: Jobs

  Los trabajos ETL son ficheros XML, desarrollados por Five, que especifican las tareas de extracción, transformación y carga que el motor de transformación debe realizar sobre los datos del usuario.

  Estos trabajos son los que implementan la importación de datos con los diferentes módulos o plugins de smartBI (Excel, CSV, ContaPlus, Sage, etc).

.. topic:: MySQL

  MySQL es un prerequisito de la aplicación; no se distribuye junto con el resto de componentes, sino que debe estar preinstalado en el servidor del cliente. La aplicación soporta la versión 5.7.7 o posterior de MySQL.

.. [#mysql] https://dev.mysql.com/downloads/mysql/
.. [#node] https://nodejs.org/en
.. [#go] https://golang.org
.. [#pentaho] http://community.pentaho.com/projects/data-integration

Estructura de directorios
-------------------------

A excepción de MySQL, que se trata como un prerrequisito, el resto de componentes de la aplicación se almacenan en un único directorio del servidor del cliente, que por convención es **C:\\smartBI** (aunque se puede cambiar). La estructura de directorios dentro de esta carpeta es la siguiente:

- **C:\\smartBI\\java8**: JRE de Java 8.
- **C:\\smartBI\\pentaho**: Ficheros del componente Pentaho.
- **C:\\smartBI\\jobs**: Ficheros XML que describen los trabajos.
- **C:\\smartBI\\service**:

  - **C:\\smartBI\\service\\svc**: Ficheros del componente serman.
  - **C:\\smartBI\\service\\current**: Ficheros del componente uploader.

- **C:\\smartBI\\config**: Configuración de la aplicación.
- **C:\\smartBI\\logs**: Logs de la aplicación.
- **C:\\smartBI\\uploads**: Ficheros subidos por el usuario para su proceso.
- **C:\\smartbi\\certs**: Certificados utilizados para proteger las comunicaciones mediante TLS.
- **C:\\smartbi\\models**: Definición de conectores y queries.

Ninguno de los componentes que se almacena en C:\\smartBI deja residuos de instalación o desinstalación en otras carpetas, o en el registro de Windows. Todas las configuraciones, con la excepción de MySQL, se almacenan en c:\\smartBI\\config.

Los ficheros de log en C:\\smartBI\\logs se rotan automáticamente, y tanto el tamaño de cada fichero como el número de ficheros previos a conservar son configurables.

Servicios
---------

La aplicación requiere que en todo momento, los siguientes servicios estén activos en el cohete:

- MySQL57: Base de datos, accesible a través del puerto **13306** del servidor.
- smartBI: Servidor HTTPS REST, accesible a través del puerto **3000** del servidor.

Para garantizar que la aplicación funciona como debe, debe cumplirse que ambos servicios tengan arranque automático, estén activos, y los puertos que utilizan estén libres en el momento de arranque. Durante la ejecución, podrá comprobarse mediante *netstat* cómo los puertos TCP **13306** y **3000** están siendo utilizados por los servicios.
