Logs de servicio
================

Los logs de servicio se guardan en la carpeta *C:\\smartBI\\logs*. Existen 4 logs principales:

.. _control_log:

Control.log
-----------

En este fichero de log escribe el gestor de servicio (*serman*) cada vez que realiza una acción sobre el servicio (iniciarlo, detenerlo, instalarlo, desinstalarlo...). Este fichero se podrá consultar para comprobar si el servicio ha arrancado correctamente.

.. code-block:: bash

  2016/11/25 12:18:49 Running interactively with args [c:\smartBI\service\svc\serman.exe]
  2016/11/25 12:18:49 Received Start signal
  2016/11/25 12:18:49 Starting runner for service smartBI REST service

.. _access_log:

Access.log
----------

Este fichero contiene el registro de todas las peticiones atendidas por el servidor REST, en un formato muy similar al de otros servidores web populares (Apache, Nginx):

- IP del equipo que lanzó la petición (siempre debe ser el backend)
- Fecha y hora
- Método (GET, POST...)
- Ruta
- Código devuelto
- Longitud de la respuesta

.. code-block:: bash

  A.B.C.D - - [25/Nov/2016:12:19:50 +0100] "GET /remote/api/run HTTP/1.1" 200 103
  A.B.C.D - - [25/Nov/2016:12:33:36 +0100] "GET /remote/api/run HTTP/1.1" 200 103
  A.B.C.D - - [25/Nov/2016:12:51:55 +0100] "GET /remote/api/run HTTP/1.1" 200 103
  A.B.C.D - - [26/Nov/2016:20:33:51 +0100] "POST /remote/api/files HTTP/1.1" 200 48
  A.B.C.D - - [26/Nov/2016:20:33:55 +0100] "GET /remote/api/run HTTP/1.1" 200 103

.. _error_log:

Error.log
---------

Este fichero contiene los mensajes de registro generados por el servidor REST (*uploader*) cuando no es capaz de satisfacer una petición recibida. Proporcionan información adicional a la que se puede encontrar en el fichero :ref:`access_log`.

.. code-block:: bash

  2016/11/25 12:18:50 Config section callhome successfully loaded
  2016/11/25 12:18:50 Loading config section uploads
  2016/11/25 12:18:50 Config section uploads successfully loaded
  2016/11/25 12:18:50 BEWARE! Server is running in unprotected mode
  2016/11/25 12:18:50 BEWARE! ALL ENDPOINTS ARE UNPROTECTED!
  2016/11/25 12:18:50 Starting server
  2016/11/29 12:47:04 http: TLS handshake error from 169.54.244.89:43271: tls: first record does not look like a TLS handshake

.. _command_log:

Command.log
-----------

Este fichero contiene el resultado de ejecutar la aplicación de data-integration sobre los datos proporcionados por el usuario. En caso de que el asistente web muestre al usuario un error al ejecutar la carga de algún fichero, los errores concretos que se hayan producido se mostrarán aquí.

.. code-block:: bash

  2016/11/26 20:34:33 - stage1_bootstrap - Finished job entry [Stage 2: Ejecuta plugin] (result=[true])
  2016/11/26 20:34:33 - stage1_bootstrap - Finished job entry [Lee configuracion] (result=[true])
  2016/11/26 20:34:33 - stage1_bootstrap - Job execution finished
  2016/11/26 20:34:33 - Kitchen - Finished!
  2016/11/26 20:34:33 - Kitchen - Start=2016/11/26 20:34:00.682, Stop=2016/11/26 20:34:33.372
  2016/11/26 20:34:33 - Kitchen - Processing ended after 32 seconds.
  OpenJDK 64-Bit Server VM warning: ignoring option MaxPermSize=256m; support was removed in 8.0
