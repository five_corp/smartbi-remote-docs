Desinstalación
==============

Todos los ficheros y configuraciones de la aplicación se almacenan en la carpeta c:\\smartBI y en la base de datos. No se hace uso de otras carpetas ni del registro de Windows. Gracias a esto, para desinstalar la aplicación es suficiente con:

1. Detener y desinstalar el servicio smartBI con las órdenes:

  ``C:\smartBI\Service\svc\serman -service stop``

  ``C:\smartBI\Service\svc\serman -service uninstall``

2. Eliminar el directorio c:\\smartBI

3. Desinstalar el servicio MySQL, si el cliente ya no lo necesita.

4. Eliminar las reglas de cortafuegos que permiten el acceso a los puertos de la aplicación (3000, 13306).
