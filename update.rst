Actualización
=============

La actualización de cualquiera de los componentes del servicio se realizará siempre siguiendo el mismo procedimiento.

1. Detención del servicio **smartBI**.

2. Descompresión del paquete o los paquetes de actualización proporcionados por Five en la ruta correspondiente:

  ================ =========================== ====================
  Componente       Fichero de distribucion     Ruta
  ================ =========================== ====================
  Java JRE 8       java8.zip [#java8.zip]_     C:\\smartBI\\java8
  Pentaho          pentaho.zip [#pentaho.zip]_ C:\\smartBI\\pentaho
  Jobs             jobs.zip [#jobs.zip]_       C:\\smartBI\\jobs
  Serman, Uploader service.zip [#service.zip]_ C:\\smartBI\\service
  ================ =========================== ====================

  Los paquetes de actualización pueden ser completos (reemplazar todos los ficheros de cada carpeta) o parciales (reemplazar solo una parte de los ficheros), para reducir el tráfico de datos y el tiempo de actualización.

.. [#java8.zip] https:/smartbi.acens.net/public/java8.zip
.. [#pentaho.zip] https:/smartbi.acens.net/public/pentaho.zip
.. [#jobs.zip] https:/smartbi.acens.net/public/jobs.zip
.. [#service.zip] https:/smartbi.acens.net/public/service.zip

3. Actualización de las configuraciones mediante la ejecución del comando
**bootstrap.exe -update** (localizado en C:\\smartBI\\service\\current).

4. Arranque del servicio **smartBI**.

En caso de actualización de MySQL a una versión soportada por Five, se detendrá el servicio smartBI antes de realizar la actualización, y se reiniciará tras concluir. En este caso no será necesaria ninguna descompresión de paquetes o actualización de configuraciones.

